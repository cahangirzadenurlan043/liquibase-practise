package com.example.LiquBasePractise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquBasePractiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquBasePractiseApplication.class, args);
	}

}

