package com.example.LiquBasePractise.service.impl;

import com.example.LiquBasePractise.model.entity.Person;
import com.example.LiquBasePractise.model.dto.PersonRequestDto;
import com.example.LiquBasePractise.repository.PersonRepository;
import com.example.LiquBasePractise.service.IPersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonService implements IPersonService {
    private final PersonRepository personRepository;

    public ResponseEntity<String> savePerson(PersonRequestDto personRequestDto){
        Person person = Person.builder()
                .name(personRequestDto.getName())
                .age(personRequestDto.getAge())
                .build();
        personRepository.save(person);

        return ResponseEntity.ok("Save is successfully!");
    }

    public List<Person> getAllPersons(){
        return personRepository.findAll();
    }

}
