package com.example.LiquBasePractise.service;

import com.example.LiquBasePractise.model.dto.PersonRequestDto;
import com.example.LiquBasePractise.model.entity.Person;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPersonService {
    ResponseEntity<String> savePerson(PersonRequestDto personRequestDto);
    List<Person> getAllPersons();
}
