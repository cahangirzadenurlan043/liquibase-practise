package com.example.LiquBasePractise.controller;

import com.example.LiquBasePractise.model.entity.Person;
import com.example.LiquBasePractise.model.dto.PersonRequestDto;
import com.example.LiquBasePractise.service.IPersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class PersonController {
    private final IPersonService personService;

    @GetMapping("/persons")
    public List<Person> getAllPerson(){
        return personService.getAllPersons();
    }

    @PostMapping("/save")
    public ResponseEntity<String> savePerson(@RequestBody PersonRequestDto personRequestDto){
        System.out.println(personRequestDto.toString());
        return personService.savePerson(personRequestDto);
    }
}
